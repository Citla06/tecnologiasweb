<?php
    error_reporting(0);

    //Ejercicio 1
    echo '<h1> Ejercicio 1 </h2>';
    echo 'La variable <b>$_myvar</b> es válida porque inicia con un underscore <br>';
    echo 'La variable <b>$_7var</b> es válida porque inicia con un underscore <br>';
    echo 'La variable <b>myvar</b> no válida porque no tiene el signo $ <br>';
    echo 'La variable <b>$myvar</b> es válida porque tiene el signo $ <br>';
    echo 'La variable <b>$var7</b> es válida porque inicia con letras y en seguida esta un numero <br>';
    echo 'La variable <b>$_element1</b> es válida porque inicia con un underscore <br>';
    echo 'La variable <b>$house*5</b> no es válida porque * no es un caracter ASCII <br>';

    // Ejercicio2
    echo '<h1> Ejercicio 2 </h2>';
    $a = "ManejadorSQL";
    $b = 'Mysql';
    $c = &$a;
    echo 'a = '. $a . "<br>";
    echo 'b = '. $b . "<br>";
    echo 'c = '. $c . "<br>";

    $a = "PHP server";
    $b = &$a;

    echo '<br> a = '. $a . "<br>";
    echo 'b = '. $b . "<br>";
    echo 'c = '. $c . "<br>";
    echo 'En el segundo bloque la varible "$a" cambio su valor a "PHP server", las variables "$b" y "$c" tienen referencia a la varible "$a", por eso cambia su valor.'; 
    unset($a, $b, $c);

    /// Ejercicio 3
    echo '<h1> Ejercicio 3 </h2>';
    $a = "PHP5";
    echo 'a = ' . $a . '  >> tipo: '. gettype($a).'<br>';
    $z[] = &$a;
    echo "z = ";                 
    print_r($z);
    echo '  >> tipo: '. gettype($z).'<br>';
    $b = "5a version de PHH";
    echo "b = " . $b . '  >> tipo: '. gettype($b). '<br>';
    $c = $b*10;
    echo 'c = '.$c . '  >> tipo: '. gettype($c).'<br>';
    $a .= $b;
    echo 'a = ' . $a . '  >> tipo: '. gettype($a).'<br>';
    $b *= $c;
    echo "b = " . $b . '  >> tipo: '. gettype($b). '<br>';
    $z[0] = "MySQL";
    echo "z = ";
    print_r($z);
    echo '  >> tipo: '. gettype($z).'<br>';

    // Ejercicio 4
    echo '<h1> Ejercicio 4 </h2>';
    echo "a = ". $GLOBALS['a'] ."<br>";
    echo "b = ". $GLOBALS['b'] ."<br>";
    echo "c = ". $GLOBALS['c'] ."<br>";
    echo "z = ";
    print_r($GLOBALS['z']);
    unset($a, $b, $c, $z);

    // Ejercicio 5
    echo '<h1> Ejercicio 5 </h2>';
    $a = "7 personas";
    $b = (integer) $a;
    $a = "9E3";
    $c = (double) $a;
    echo 'a = ' . $a . '<br>';
    echo 'b = ' . $b . '<br>';
    echo 'c = ' . $c . '<br>';
    unset($a, $b, $c);

    //Ejercicio 6
    echo '<h1> Ejercicio 6 </h2>';
    $a = "0";
    $b = "TRUE";
    $c = FALSE;
    $d = ($a OR $b);
    $e = ($a AND $c);
    $f = ($a XOR $b);

    function tipo_boolean($opcion)
    {
        if(is_bool($opcion))
        {
            echo "Es booleana <br>";
        }
        else
        {
            echo "No es booleana <br>";
        }
    }
    tipo_boolean($a);
    tipo_boolean($b);
    tipo_boolean($c);
    tipo_boolean($d);
    tipo_boolean($e);
    tipo_boolean($f);

    var_dump($a, $b, $c, $d, $e, $f);
    echo '<br> <br>';
    echo "Funcion var_export para transformar el valor booleano <br>";
    $c = var_export($c,true);
    echo 'c = ' .$c. '<br>';
    $e = var_export($c,true);
    echo 'e = ' .$e. '<br>';

    //Ejercicio 7
    echo '<h1> Ejercicio 7 </h2>';

    
    echo $_SERVER['SERVER_SOFTWARE'] . '<br>';
    echo '<b>Sistema operativo: </b>'.$_SERVER['HTTP_USER_AGENT'].'<br>';
    echo '<b>Servidor: </b>'.$_SERVER['SERVER_NAME'] . '<br>';
    echo '<b>Idioma navegador: </b>'.$_SERVER['HTTP_ACCEPT_LANGUAGE'] . '<br>';
?>